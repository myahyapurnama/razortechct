package purnama.yahya.razortechct

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context,DB_Name,null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tCatatan = "create table catatan(id_catatan integer primary key autoincrement, nama text not null, id_kategori int not null)"
        val tKategori = "create table kategori(id_kategori integer primary key autoincrement, nama_kategori text not null)"
        db?.execSQL(tCatatan)
        db?.execSQL(tKategori)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "razortechxx"
        val DB_Ver = 1
    }
}