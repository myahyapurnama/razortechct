package purnama.yahya.razortechct

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragKategori : FragmentKategori
    lateinit var fragMhs : FragmentCatatan
    lateinit var ft : FragmentTransaction
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragKategori = FragmentKategori()
        fragMhs = FragmentCatatan()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDBObject() : SQLiteDatabase {
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemKategori ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragKategori).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemCatatan ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragMhs).commit()
                FrameLayout.setBackgroundColor(Color.argb(245,225,255,255))
                FrameLayout.visibility = View.VISIBLE
            }
            R.id.itemHome -> FrameLayout.visibility = View.GONE
        }
        return true
    }
}
