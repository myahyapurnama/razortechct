package purnama.yahya.razortechct

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_cttn.*
import kotlinx.android.synthetic.main.frag_data_cttn.view.*

@Suppress("DEPRECATION")
class FragmentCatatan : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaProdi = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteMhs ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage("Yakin akan menghapus data ini?")
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnInsertMhs ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnUpdateMhs ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnCari ->{
                showDataMhs(edNamaMhs.text.toString())
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var idMhs : String = ""
    var namaProdi : String = ""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_cttn,container,false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteMhs.setOnClickListener(this)
        v.btnInsertMhs.setOnClickListener(this)
        v.btnUpdateMhs.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.btnCari.setOnClickListener(this)
        v.lsMhs.setOnItemClickListener(itemClick)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataMhs("")
        showDataProdi()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idMhs = c.getString(c.getColumnIndex("_id"))
        v.edNimMhs.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaMhs.setText(c.getString(c.getColumnIndex("nama")))
    }

    fun showDataMhs(namaMhs : String){
        var sql = ""
        if(!namaMhs.trim().equals("")){
            sql = "select c.id_catatan as _id, c.nama, k.nama_kategori from catatan c, kategori k " +
                    "where c.id_kategori = k.id_kategori and c.nama like '%$namaMhs%'"
        }else{
            sql = "select c.id_catatan as _id, c.nama, k.nama_kategori from catatan c, kategori k " +
                    "where c.id_kategori = k.id_kategori order by c.nama asc"
        }
        val c : Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_cttn,c,
            arrayOf("_id","nama","nama_kategori"), intArrayOf(R.id.txNimMhs, R.id.txNamaMhs, R.id.txNamaPS),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsMhs.adapter = lsAdapter
    }

    fun showDataProdi(){
        val c : Cursor = db.rawQuery("select nama_kategori as _id from kategori order by nama_kategori asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun insertDataMhs(nim : String, namaMhs: String, id_prodi : Int){
        var sql = "insert into catatan (id_catatan, nama, id_kategori) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim, namaMhs, id_prodi))
        showDataMhs("")
    }

    fun updateDataMhs(nim : String, namaMhs: String, id_prodi : Int){
        var sql = "UPDATE catatan SET nama=?, id_kategori=? WHERE id_catatan=?"
        db.execSQL(sql, arrayOf(namaMhs, id_prodi, nim))
        showDataMhs("")
    }

    fun deleteDataMhs(nim : String){
        var sql = "DELETE FROM catatan WHERE id_catatan=?"
        db.execSQL(sql, arrayOf(nim))
        showDataMhs("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kategori from kategori where nama_kategori='$namaProdi'"
        val c : Cursor = db.rawQuery(sql, null)
        if (c.count>0){
            c.moveToFirst()
            insertDataMhs(v.edNimMhs.text.toString(), v.edNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_kategori")))
            v.edNimMhs.setText("")
            v.edNamaMhs.setText("")
        }
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_kategori from kategori where nama_kategori='$namaProdi'"
        val c : Cursor = db.rawQuery(sql, null)
        if (c.count>0){
            c.moveToFirst()
            updateDataMhs(v.edNimMhs.text.toString(), v.edNamaMhs.text.toString(),
                c.getInt(c.getColumnIndex("id_kategori")))
            v.edNimMhs.setText("")
            v.edNamaMhs.setText("")
        }
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMhs(v.edNimMhs.text.toString())
        v.edNimMhs.setText("")
        v.edNamaMhs.setText("")
    }
}